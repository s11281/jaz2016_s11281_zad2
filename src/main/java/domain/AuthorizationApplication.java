package domain;
public class AuthorizationApplication {
	private String username;
	private String password;
	private String email;
	private boolean normaluser;
	private boolean premiumuser;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isNormaluser() {
		return normaluser;
	}
	public void setNormaluser(boolean normaluser) {
		this.normaluser = normaluser;
	}
	public boolean isPremiumuser() {
		return premiumuser;
	}
	public void setPremiumuser(boolean premiumuser) {
		this.premiumuser = premiumuser;
	}
	
	
	
}