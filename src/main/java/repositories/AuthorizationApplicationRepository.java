package repositories;

import domain.AuthorizationApplication;

public interface AuthorizationApplicationRepository {
	
	void add(AuthorizationApplication application);
	int count();
}
