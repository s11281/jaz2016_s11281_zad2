package repositories;

import java.util.ArrayList;
import java.util.List;
import domain.AuthorizationApplication;

public class DummyAuthorizationApplicationRepository implements AuthorizationApplicationRepository{
	
	private static List<AuthorizationApplication> db = new ArrayList<AuthorizationApplication>();
	
	@Override
	public void add(AuthorizationApplication application){
		db.add(application);
	}
	
	@Override
	public int count(){
		return db.size();
	}

}
