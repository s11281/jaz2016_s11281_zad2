package web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.AuthorizationApplication;
import repositories.AuthorizationApplicationRepository;
import repositories.DummyAuthorizationApplicationRepository;

/**
 * Servlet implementation class AddUserServlet
 */
@WebServlet("/AddUserServlet")
public class AddUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddUserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		AuthorizationApplication application = retrieveApplicationFromRequest(request);
		AuthorizationApplicationRepository repository = new DummyAuthorizationApplicationRepository();
		repository.add(application);
		response.sendRedirect("success.jsp");
		
	}
	
	private AuthorizationApplication retrieveApplicationFromRequest(HttpServletRequest request){
		AuthorizationApplication result = new AuthorizationApplication();
		result.setUsername(request.getParameter("username"));
		result.setPassword(request.getParameter("password"));
		result.setEmail(request.getParameter("email"));
		result.setNormaluser(false);
		result.setPremiumuser(false);
		return result;
		
	}

}
